Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%c)

%! encoding: utf-8


===Exemple d'installation détaillée d'OpenBSD===[install]

Cette section n'est là que pour rassurer les plus réticents. Pour des
informations plus complètes, n'hésitez pas à consulter 
la [documentation d'OpenBSD http://www.openbsd.org/faq/faq4.html].

On télécharge tout d'abord l'image d'installation de la dernière version d'OpenBSD, qui
est, à l'heure où j'écris ces lignes, la OBSDVERSION.  Une liste de 
miroirs est disponible [en ligne http://www.openbsd.org/ftp.html].

- Pour installer à partir d'un cdrom, choisissez installOBSDMINVERSION.iso puis gravez-la sur
  le CD.
- Pour installer à partir d'une clé USB, choisissez installOBSDMINVERSION.fs.  Pour préparer une
  clé USB, utilisez l'outil ``dd`` (remplacez ``/dev/sdb`` par le chemin
  vers votre clé USB). Par exemple sur un système GNU/Linux : 

```
# dd if=/location/install*.fs of=/dev/sdb bs=1M 
```


Les utilisateurs de Windows pourront faire la même chose avec le logiciel 
[rufus https://rufus.akeo.ie/].
    
Et hop, on insère tout ça dans le lecteur du PC, puis on redémarre en
choisissant de démarrer sur le bon media (//boot// en anglais). Repérez les messages qui s'affichent du
type ``F12 : Boot Menu`` ou ``F7 : Setup`` qui indiquent la touche
permettant de configurer le média sur lequel l'ordinateur démarre en priorité.

Le premier écran nous propose d'ajouter des options pour démarrer OpenBSD. On n'en a pas besoin, on appuie donc juste sur "Entrée" : 

```
>> OpenBSD/amd64 CDBOOT 3.26
boot> 
```


On lance l'installation avec I : 

```
Welcome to the OpenBSD/amd64 6.1 installation program.
(I)nstall, (U)pgrade, (A)utoinstall or (S)hell?
```


Les valeurs proposées pour l'installation sont largement suffisantes
dans la plupart des cas. Les choix par défaut sont indiqués entre crochets : 
``[choix]``. À chaque fois, des exemples sont disponibles avec ``'?'``.

On choisit une disposition de clavier : fr 

```
At any prompt except password prompts you can escape to a shell by typing '!'.
Default answers are shown in []'s and are selected by pressing RETURN.  
You can exit this program by pressing Control-C, 
but this can leave your system in an inconsistent state.

Choose your keyboard layout ('?' or 'L' for list) [default] fr
```


On choisit ensuite un nom de machine, par exemple "maitre.&NDD" "maitre".

```
System hostname? (short form, e.g. 'foo')
```


Ensuite, on configure la connexion à Internet. Notez qu'il n'est pas obligatoire
d'avoir un accès en ligne si vous avez choisi une image install*.*. Toutefois,
un serveur déconnecté, c'est presque antinomique.

Vous avez la possibilité de définir une [IP statique #IP] pour votre serveur ou bien
utiliser la configuration par dhcp par facilité.


```
Available network interfaces are: em0 vlan0.
Which network interface do you wish to configure? (or 'done') [em0]
IPv4 address for em0? (or 'dhcp' or 'none') [dhcp]
DHCPDISCOVER on em0 - interval 1
DHCPOFFER from 10.0.2.2 (52:55:01:00:02:02)
DHCPREQUEST on em0 to 255.255.255.255
DHCPACK from 10.0.2.2 (52:55:01:00:02:02)
bound to 10.0.2.15 -- renewal in 43200 seconds.
```


Pour avoir une IPv6, choisissez ``rtsol`` le moment venu pour en avoir une
automatiquement. Vous souhaiterez certainement configurer cette partie
[manuellement #reseau] par la suite cela dit.

```
IPv6 address for em0? (or 'rtsol' or 'none') [none] rtsol
Available network interfaces are: em0 vlan0.
Which network interface do you wish to configure? (or 'done') [done]
```


La configuration réseau se termine par le domaine DNS. Laissez le choix par
défaut à moins d'avoir un besoin très particulier : 

```
DNS domain name? (e.g. 'bar.com') [my.domain]
Using DNS nameservers at 10.0.2.3
```


Ensuite, l'installateur vous demande le mot de passe de l'administrateur
système dont le petit nom est //root//. 
Choisissez [un mot de passe robuste #mdp].

```
Password for root account? (will not echo)
Password for root account? (again)
```


Vous pouvez ensuite faire en sorte que SSH soit lancé par défaut au démarrage.
C'est conseillé pour un serveur afin de pouvoir l'administrer sans écran à
partir d'un autre ordinateur.

On nous demande si un serveur X sera utilisé (session graphique) : ce n'est
absolument pas nécessaire pour un serveur. Autant attribuer toutes les
ressources aux calculs autres que l'affichage.

Il est inutile de changer la console par défaut, laissez "no".

```
Password for root account? (will not echo)
Password for root account? (again)
Start sshd(8) by default? [yes]
Do you expect to run the X Window System? [yes] no
Change the default console to com0? [no]
```


Vous pouvez ajouter un utilisateur si vous le souhaitez en entrant son login.
Vous n'êtes pas obligés, mais je vous le conseille pour vous connecter à votre
serveur via SSH. Ainsi, vous éviterez d'utiliser le compte root.

```
Setup a user? (enter a lower-case loginname, or 'no') [no] luffy
Full name for user luffy? [luffy]
Password for user luffy? (will not echo)
Password for user luffy? (again)
```


On vous demande ensuite si vous voulez autoriser l'utilisateur "root" à se
connecter via SSH. **C'est une très mauvaise idée**, puisque cet utilisateur a
tous les droits et est la cible des pirates. Choisissez "no".

```
WARNING: root is targeted by password guessing attacks, pubkeys are safer.
Allow root ssh login? (yes, no, prohibit-password) [no]
```


Nous passons ensuite à la configuration du fuseau horaire. 

```
What timezone are you in? ('?' for list) [Europe/Paris]
```


C'est parti pour le choix du disque : 

```
Which disk is the root disk? ('?' for details) [wd0]
```


Après avoir appuyé sur Entrée, vous voyez : 

```
Disk: wd0      geometry: 2610/255/63 [41943040 Sectors]
Offset: 0      Signature: 0xAA55
           Starting         Ending     LBA Info:
 #: id     C   H   S -      C   H  S [    start:     size ]
-----------------------------------------------------------------
 0: 00     0   0   0 -      0   0  0 [        0:        0 ] unused
 1: 00     0   0   0 -      0   0  0 [        0:        0 ] unused
 2: 00     0   0   0 -      0   0  0 [        0:        0 ] unused
*3: A6     0   1   2 -   2609 254 63 [       64: 41929586 ] OpenBSD
Use (W)hole disk MBR, whole disk (G)PT, (O)penBSD area or (E)dit? [W]
```

Nous voilà à la partie sans doute la plus complexe : le partitionnement. Sachez
que celui par défaut proposé par l'installateur conviendra dans la
majorité des cas.

Je tape donc "W" (ou "G") dans la suite pour utiliser le disque entier, puis "A" pour le partitionnement automatique.

Vous pouvez modifier le partitionnement par défaut en tapant "E".

```
Setting OpenBSD MBR partition to whole wd0...done.
The auto-allocated layout for wd0 is :
#            size     offset  fstype [fsize bsize   cpg]
  a:   738.1M         64  4.2BSD   2048 16384     1 # /
  b:   223.8M    1511648  swap
  c: 20480.0M          0  unused
  d: 172.9.1M    1969888  4.2BSD   2048 16384     1 # /tmp
  e:  1791.0M    4372032  4.2BSD   2048 16384     1 # /var
  f:  1558.1M    8040032  4.2BSD   2048 16384     1 # /usr
  g:   906.8M   11230976  4.2BSD   2048 16384     1 # /usr/X11R6
  h:  3364.2M   13088192  4.2BSD   2048 16384     1 # /usr/local
  i:  1287.2M   19977984  4.2BSD   2048 16384     1 # /usr/src
  j:  1826.5M   22614208  4.2BSD   2048 16384     1 # /usr/obj
  k:  7604.8M   26354784  4.2BSD   2048 16384     1 # /home
Use (A)uto layout, (E)dit auto layout, or create (C)ustom layout? [a] e
```


Ensuite, vous pouvez lire le
[manuel de ``disklabel`` http://man.openbsd.org/OpenBSD-current/man8/disklabel.8]
si vous êtes perdus. Il faut en général indiquer une action
(avec une lettre) à
réaliser sur la partition où il faudra l'effectuer. 

Par exemple : 
la suite ``d f`` permet de supprimer la partition f, qui était ``/usr``, et 
``d k`` pour supprimer le ``/home``. 

Ensuite, taper ``a f`` permet de recréer cette
partition, et d'en définir la taille. Notez
que vous pouvez définir une taille en pourcentage du disque (par exemple ``50%``) ou
en pourcentage de l'espace libre restant (``50&``). Ici, je vais réduire le
``/home`` au profit de ``/usr`` qui prendra 75% de l'espace restant.
De la même façon avec ``a k``, on recrée ``/home`` à la taille souhaitée.


```
Use (A)uto layout, (E)dit auto layout, or create (C)ustom layout? [a] e
Label editor (enter '?' for help at any prompt)
> d f
> d k
> a f
offset: [26354784]
size: [15574866] 75&
FS type: [4.2BSD]
mount point: [none] /usr
> a k
offset: [38035904]
size: [3893746]
FS type: [4.2BSD]
mount point: [none] /home
```



À tous moments, vous pouvez taper ``p`` pour afficher les partitions actuelles,
histoire de voir où vous en êtes.

```
> p 
OpenBSD area: 64-41929650; size: 41929586; free: 3190962
The auto-allocated layout for wd0 is :
#           size       offset  fstype [fsize bsize   cpg]
  a:     1511584           64  4.2BSD   2048 16384     1 # /
  b:      458240      1511648  swap
  c:    41943040            0  unused
  d:     2402144      1969888  4.2BSD   2048 16384     1 # /tmp
  e:     3668000      4372032  4.2BSD   2048 16384     1 # /var
  f:    11681120     26354784  4.2BSD   2048 16384     1 # /usr
  g:     1857216     11230976  4.2BSD   2048 16384     1 # /usr/X11R6
  h:     6889792     13088192  4.2BSD   2048 16384     1 # /usr/local
  i:     2636224     19977984  4.2BSD   2048 16384     1 # /usr/src
  j:     3740576     22614208  4.2BSD   2048 16384     1 # /usr/obj
  k:     3893728     38035904  4.2BSD   2048 16384     1 # /home
```


Quelques points à garder en tête : 
- La partition "``b``" est réservée au swap.
- La lettre "``c``" est réservée, elle représente tout le disque. 
- Vos sites web seront enregistrés dans ``/var``. Songez à lui attribuer une
  place suffisante.
- Vous pouvez définir des tailles en ajoutant une unité (``50G``), un
  pourcentage du disque (``5%``) ou un pourcentage de l'espace libre restant
  (``100&``).


Taper ``q`` permet de valider les changements.

```
> q
Write new label?: [y]
newfs: reduced number of fragments per cylinder group from 94472 to 94096 to
enlarge last cylinder group
/dev/rwd0a: 738.1MB in 1511584 sectors of 512 bytes
5 cylinder groups of 114.559MB, 7334 blocks, 14720 inodes each
...
...
/dev/wd0a (8c0364801ae0817e.a) on /mnt type ffs  \
    (rw, asynchronous,local, nodev, nosuid)
/dev/wd0k (8c0364801ae0817e.k) on /mnt/home type ffs  \
    (rw, asynchronous,local, nodev, nosuid)
...
...
```


	STOP ! Je n'ai aucune idée de l'espace à consacrer à mes partitions. Un coup
	de main svp ?


Si vous doutez, je vous propose la répartition suivante très simple : 

- 10G pour ``/usr/local`` seront amplement suffisants pour contenir les
  programmes installés.
- Entre 10G et 50G pour ``/var`` qui contiendra vos sites et les 
[journaux #logs]. Vous pouvez attribuer encore plus d'espace si vous pensez en
avoir besoin.
- L'espace restant ira pour la partition racine et donc tout le reste : ``/``.



Enfin l'installation des composants du système peut commencer. Choisissez
``cd0`` si vous installez avec une image install*.* ou bien ``http`` sinon.


Pour ajouter un
set, saisissez simplement son nom, par exemple ``gamesOBSDMINVERSION.tgz``. Pour retirer un set,
saisissez son nom précédé du signe moins : ``-gamesOBSDMINVERSION.tgz``. En cas de doute,
laissez coché le set proposé.

```
Let's install the sets!
Location of sets (cd0 disk http or 'done') [http]
HTTP proxy URL? (e.g. ’http://proxy:8080’, or ’none’) [none]
HTTP Server? (hostname, list#, ’done’ or ’?’) [ftp.fr.openbsd.org]
Server directory? [pub/OpenBSD/6.1/amd64]

Select sets by entering a set name, a file name pattern or ’all’. De-select
sets by prepending a ’-’ to the set name, file name pattern or ’all’. Selected
sets are labelled ’[X]’.

  [X] bsd     [X] base61.tgz  [X] game61.tgz    [X] xfont61.tgz
  [X] bsd.rd  [X] comp61.tgz  [X] xbase61.tgz   [X] xserv61.tgz
  [ ] bsd.mp  [X] man61.tgz   [X] xshare61.tgz

Set name(s)? (or ’abort’ or ’done’) [done]
```


``bsd.mp`` est le noyau optimisé pour les systèmes multi-processeurs, et ``bsd``
le noyau classique. ``bsd.rd`` est utilisé pour les mises à jour ou le dépannage
car il se charge en mémoire. Vous n'en aurez pas forcément besoin.

Si vous installez les sets à partir du CD, un avertissement sur la signature
apparaît. Rien d'inquiétant, vous pouvez dans ce cas continuer.

```
Directory does not contain SHA256.sig. Continue without verification? [no] yes
```


L'installation avance 

```
Get/Verify SHA256.sig    100% |**************************| 2152        00:00
Signature Verified
Get/Verify bsd           100% |**************************| 10423 KB    00:14
Get/Verify bsd.rd        100% |**************************| 9215  KB    00:11
...
...
Installing base61.tgz    100% |**************************| 52181 KB
...
...
Installing xserv61.tgz    100% |**************************| 22686 KB
Location of sets? (cd0 disk http or 'done' [done]
```


Pour terminer, validez le "done" : 

```
Saving configuration files...done.
Making all device nodes...done.

CONGRATULATIONS! Your Openbsd install has been successfully 
completed!
To boot the new system, enter 'reboot' at the command prompt.
When you login to your new system the first time, please read your 
mail using the 'mail' command.

#
```


Et voilà, l'installation est terminée, on peut redémarrer avec la commande ``reboot``.


Lors du premier démarrage, éditez le fichier ``/etc/installurl`` puis
assurez-vous d'avoir une ligne contenant le miroir de téléchargement choisi :

```
https://ftp.fr.openbsd.org/pub/OpenBSD/
```

ou : 

```
ftp://ftp2.fr.openbsd.org/pub/OpenBSD/
```




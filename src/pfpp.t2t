



==Aller plus loin avec pf==[pfpp]


===Blacklist d'IP nuisibles===
Certaines adresses IP sont connues pour être les sources d'attaques. Vous pouvez configurer
pf pour qu'il rejette automatiquement toutes les requêtes venant de ces IP.

Pour nous faciliter la vie, Stéphane HUC s'est donné la peine d'écrire des
outils qui rassemblent des listes d'IP nuisibles de plusieurs sources. Merci
qui ? Merci Stéphane !

Son projet se trouve à 
[cette adresse https://framagit.org/BlackLists/BlockZones].

Il contient des listes que vous
pourrez utiliser avec d'autres logiciels que pf si vous avez l'envie d'y jeter
un oeil.

En attendant, nous allons utiliser directement les listes générées par ses bons
soins.

On commence par télécharger les listes, tout en vérifiant leur intégrité. Nous
les placerons dans un dossier ``/var/blockzones``.

```
# mkdir -p /var/blockzones
# cd /var/blockzones
# ftp https://stephane-huc.net/share/BlockZones/lists/badips_ipv4
# ftp https://stephane-huc.net/share/BlockZones/lists/badips_ipv6
# ftp https://stephane-huc.net/share/BlockZones/lists/badips_ipv4.sha512
# ftp https://stephane-huc.net/share/BlockZones/lists/badips_ipv6.sha512
```


Pour vérifier l'intégrité des données, on lance pour chaque fichier : 

```
# sha512 -C badips_ipv4.sha512 badips_ipv4
```


Le message suivant doit vous être retourné : 

```
(SHA512) badips_ipv4: OK
```


Pour utiliser ces listes, on ajoute les lignes suivantes dans le fichier
``/etc/pf.conf`` : 

```
set limit table-entries 400000
table <t_badips>  persist file "/var/blockzones/badips_ipv4"
table <t_badips6> persist file "/var/blockzones/badips_ipv6"


block quick from { <t_badips>, <t_badips6> }
```


Vous pouvez maintenant relancer le parefeu avec ``pfctl -f /etc/pf.conf``. Et
voilà, votre serveur sera nettement allégé à l'avenir.


Je vous invite à mettre à jour régulièrement les listes d'IP en mettant les
lignes précédentes dans le fichier ``/etc/weekly.local`` par exemple.

Pour compléter les listes proposées par Stéphane, vous pouvez consulter celles
que je rassemble avec le même projet ainsi que l'outil [vilain #vilain] en
suivant 
[ce lien http://yeuxdelibad.net/Logiciel-libre/OpenBSD/Listes_d_IP.html].


===Anti-bruteforce===[antibruteforce] 
Une méthode qu'utilisent les pirates pour s'en prendre à un serveur s'appelle 
"bruteforce". Ces attaques consistent à essayer toutes les combinaisons
d'identifiants/mot-de-passe possibles jusqu'à tomber sur la bonne.

Si vous avez choisi des [phrases de passe robustes #mdp], vous êtes normalement à
l'abri. Toutefois, ces attaques utilisent inutilement des ressources sur votre
serveur et peuvent potentiellement réussir un jour.

Afin de déjouer ces attaques, il faudrait prendre le temps de décortiquer les
journaux des services que vous hébergez (dans ``/var/log`` et
``/var/www/logs``) à la recherche d'échecs d'identification 
et repérer d'où viennent ces attaques pour en bannir les auteurs. 
Mais soyons honnête, c'est à la fois pénible et chronophage.

L'idéal serait un outil qui surveille les journaux à votre place et s'occupe de
mettre sur la liste noire de votre parefeu les IP des "attaquants". Ça tombe
bien, vous pouvez utiliser [vilain #vilain] ou [sshguard #sshguard] qui sont
faits pour ça.

En attendant, //pf// [intègre déjà #antibruteforcepf] une protection qui limite le nombre de
connexions sur un port pendant un temps donné. C'est très efficace et reste
léger.



====Anti-Bruteforce intégré à pf====[antibruteforcepf]

Pf dispose d'une fonctionnalité très intéressante : les "tables"
(tableaux).
Ça va nous permettre de garder en mémoire certaines adresses IP de
pirates qui tenteraient de compromettre le serveur. Par exemple, pour
protéger le service SSH, on va procéder ainsi : 

+ Création d'une table pour enregistrer les IP abusives avec notre serveur
SSH.
+ Bloquer toutes les IP qui seraient dans la table précédente, et ne pas
aller plus loin pour elles.
+ Ouvrir le port ssh, mais enregistrer dans la table précédente toutes
les IP qui tenteraient de se connecter plus de 3 fois par minute
(attaque par bruteforce).

Ça nous donne donc ceci : 

```
ssh_port = "22"
table <ssh_abuse> persist
block in log quick proto tcp from <ssh_abuse> to any port $ssh_port
pass in on $ext_if proto tcp to any port $ssh_port flags S/SA keep state \
    (max-src-conn-rate 3/60, overload <ssh_abuse> flush global)
```

Notez que nous avons enregistré le numéro du port utilisé pour le
serveur SSH. C'est inutile, car on peut mettre à la place de
``$ssh_port`` simplement ``ssh``, c'est-à-dire le nom du service.
Cependant, on peut vouloir changer le port par défaut du serveur SSH,
comme décrit dans le [chapitre sur ce service #ssh].


Voici un autre exemple pour un site web (ports http et https) : 

```
http_ports = "{ www https }"         # ports http(s)
# Si + de 40 connections toutes les 5 secondes sur les ports http(s)
# ou si elle essaie de se connecter + de 100 fois
# on ajoute l'ip pour la bloquer.
pass in on $ext_if proto tcp to any port $http_ports flags S/SA keep state \
     (max-src-conn 100, max-src-conn-rate 40/5, overload <http_abuse> flush global)
```




====En temps réel avec vilain====[vilain]
Je me permets de vous proposer un outil que j'ai écrit avec l'aide de Vincent
Delft dans le but de mettre sur
liste noire d'éventuels pirates lorsque l'attaque a lieu. Notez bien
qu'il est encore jeune et que vous devriez savoir ce que vous faîtes en
l'utilisant. Par ailleurs, vos retours d'utilisation permettront de l'améliorer.

Le script en question s'appelle ``vilain``. Vous pouvez 
suivre le lien suivant pour consulter 
[le code https://framagit.org/Thuban/vilain].

Voici comment l'installer. Tout d'abord, on télécharge l'archive dans le dossier
``/tmp`` : 

```
# cd /tmp
# ftp http://dev.yeuxdelibad.net/vilain.tgz
```


On décompresse l'archive : 

```
# tar xvzf vilain.tgz
```


Ensuite, on procède à l'installation : 

```
# cd vilain
# make install
```


Afin que ``vilain`` puisse fonctionner, ajoutez une table à la configuration de
pf afin de retenir les IP des attaquants. Éditez le fichier ``/etc/pf.conf``
pour y mettre : 

```
table <vilain_bruteforce> persist
block quick from <vilain_bruteforce>
```


Rechargez pf avec ``# pfctl -f /etc/pf.conf`` puis éditez le fichier de
configuration de vilain ``/etc/vilain.conf``. Vous y trouverez quelques exemples
de la forme suivante : 

```
[nom du gardien]
logfile = /chemin/du/journal/a/inspecter
regex = expression regulière qui retourne l'IP de l'attaquant en cas d'echec
```


D'autres options sont disponibles, comme le nombre d'échecs avant le
bannissement, les IP à ignorer s'il y a une erreur...


Afin de lancer ``vilain``, vous avez besoin de python3. Installez le paquet
``python-3.6.*`` par exemple. 


Vous pouvez maintenant activer et lancer vilain comme n'importe quel service : 

```
# rcctl enable vilain
# rcctl start vilain
```


Si vous consultez les journaux dans le fichier ``/var/log/daemon``, vous y
verrez des messages comme : 

```
Start vilain for ssh
Start vilain for ssh2
Start vilain for http401
Start vilain for smtp
Start vilain for dovecot
```

``vilain`` est actuellement en train de protéger votre serveur.



Vous pouvez si vous le souhaitez [lancer régulièrement #cron] cette commande pour
retirer les IP bannies depuis trop longtemps : 

```
pfctl -t vilain_bruteforce -T expire 86400
```


Pour voir les IP bannies, saisissez : 

```
pfctl -t vilain_bruteforce -T show
```


====En temps réel avec sshguard====[sshguard]
Contrairement à ce que son nom indique, [sshguard https://www.sshguard.net] est
en mesure de vérifier les tentatives de connexions sur plusieurs services, pas
seulement SSH.

Son installation sous OpenBSD n'est pas très compliquée à mettre en place. Comme
[vilain #vilain], vous devrez créer une table dans [pf #pf] qui contiendra
toutes les IP mises sur liste noire.
Ainsi, on ajoute dans le fichier ``/etc/pf.conf`` une nouvelle table contenant
les IP des pirates : 

```
table <sshguard> persist
block in from <sshguard>
```


Rechargez le parefeu avec ``pfctl -f /etc/pfconf``.

Maintenant, on installe sshguard comme on a l'habitude de le faire : 

```
# pkg_add sshguard
```


Nous pouvons maintenant activer et lancer sshguard : 

```
# rcctl enable sshguard
# rcctl start sshguard
```


Et voilà, votre serveur est désormais sous bonne garde.

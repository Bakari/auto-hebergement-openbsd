Rspamd
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4


===Rspamd ===
[Rspamd https://www.rspamd.com/] est un filtre antispam très rapide. 
Toutefois, il est plus compliqué à mettre en place que 
[spamassassin #spamassassin]. En effet, il est très complet et est capable de
faire du "[greylisting #greylisting]" et les signatures [DKIM #DKIM] dont on n'a pas forcément besoin
puisqu'on a déjà configuré ces éléments auparavant.

Afin de vérifier les messages, nous dirons au serveur mail de les envoyer à
//rmilter// qui fera le lien avec //rspamd//.

Ci-dessous, vous trouverez des indications pour mettre en place l'antispam
//rspamd// sans rentrer dans les détails de toutes ses fonctionnalités.

On commence par installer //rspamd//, //rmilter// et //redis// dont ils
dépendent : 

```
# pkg_add rspamd rmilter redis
```


====Configuration de rmilter====

On édite le fichier ``/etc/rmilter/rmilter.conf.local`` afin de désactiver
plusieurs fonctionnalités dont on ne veut pas se servir : les limitations, le
greylisting et la signature dkim. 

```
limits {
    enable = false;
}
greylisting {
    enable = false;
}
dkim {
    enable = false;
}
```


Ça sera tout pour //rmilter//.


====Configuration de smtpd====
Voici la configuration à entrer dans le fichier ``/etc/mail/smtpd.conf`` afin de
relayer les messages entrants dans le serveur à //rmilter//. On utilisera la
même technique des "étiquettes" que pour [spamassassin #spamassassin] : seuls
les messages étiquetés "NOSPAM" par //rspamd// seront distribués.

```

```


On termine par activer et démarrer les services : 

```
# rcctl enable dovecot rspamd redis
# rcctl start redis
# rcctl start rspamd 
# rcctl restart dovecot

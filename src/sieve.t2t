Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4

On peut laisser l'antispam faire son travail tranquille dans son coin.
Toutefois, c'est intéressant de lui indiquer les quelques mails indésirables
qu'il a pu laisser passer afin qu'il apprenne et devienne de plus en plus
efficace.

Si vous utilisez [dovecot #dovecot] afin de consulter vos messages avec un
client IMAP, il est possible de marquer comme //spam// les messages que vous
déplacez dans le dossier "Junk" (par exemple). De la même façon vous pouvez
marquer un message comme légitime si vous le retirez du dossier "Junk".

Notez que cette fonctionnalité pourra être mise en place quel que soit
l'antispam que vous utilisez.


L'installation va se dérouler en trois étapes : 
+ Installation du plugin.
+ Activation et configuration du plugin.
+ Création des scripts d'apprentissage relatifs à chaque antispam.


Pour configurer dovecot de cette façon, nous commençons par installer le plugin
//pigeonhole// qui donnera accès à //sieve//.

```
# pkg_add dovecot-pigeonhole
```


On active ce plugin en ajoutant les lignes suivantes à la fin du fichier ``/etc/dovecot/local.conf``: 

```
protocol imap {
  mail_plugins = $mail_plugins imap_sieve
}
```


Afin que les dossiers que nous allons utiliser pour classer les mails soient
automatiquement créés, on ajoute aussi ces quelques lignes : 

```
namespace inbox {
  mailbox Drafts {
    special_use = \Drafts
	auto = subscribe
  }
  mailbox Junk {
    special_use = \Junk
	auto = subscribe
  }
  mailbox Trash {
    special_use = \Trash
	auto = subscribe
  }
  mailbox Sent {
    special_use = \Sent
	auto = subscribe
  }
}
```


Enfin, toujours dans le même fichier, on configure ce plugin : 

```
plugin {
  sieve_plugins = sieve_imapsieve sieve_extprograms

  # From elsewhere to Spam folder
  imapsieve_mailbox1_name = Spam
  imapsieve_mailbox1_causes = COPY
  imapsieve_mailbox1_before = file:/usr/local/lib/dovecot/sieve/report-spam.sieve

  # From Spam folder to elsewhere
  imapsieve_mailbox2_name = *
  imapsieve_mailbox2_from = Spam
  imapsieve_mailbox2_causes = COPY
  imapsieve_mailbox2_before = file:/usr/local/lib/dovecot/sieve/report-ham.sieve

  sieve_pipe_bin_dir = /usr/local/lib/dovecot/sieve

  sieve_global_extensions = +vnd.dovecot.pipe +vnd.dovecot.execute
}
```


On crée maintenant deux fichiers dans le dossier
``/usr/local/lib/dovecot/sieve/``.

- Le premier s'appelle ``report-spam.sieve``: 

```
require ["vnd.dovecot.pipe", "copy", "imapsieve", "environment", "variables"];

if environment :matches "imap.user" "*" {
  set "username" "${1}";
}

pipe :copy "sa-learn-spam.sh" [ "${username}" ];
```

- Le second s'appelle ``report-ham.sieve``

```
require ["vnd.dovecot.pipe", "copy", "imapsieve", "environment", "variables"];

if environment :matches "imap.mailbox" "*" {
  set "mailbox" "${1}";
}

if string "${mailbox}" "Trash" {
  stop;
}

if environment :matches "imap.user" "*" {
  set "username" "${1}";
}

pipe :copy "sa-learn-ham.sh" [ "${username}" ];
```


On fait maintenant en sorte que //sieve// tienne compte de ces fichiers avec les
commandes suivantes : 

```
sievec /usr/local/lib/dovecot/sieve/report-spam.sieve
sievec /usr/local/lib/dovecot/sieve/report-ham.sieve
```


Ces deux fichiers font appel à deux scripts : ``sa-learn-spam.sh`` et
``sa-learn-ham.sh``. Le contenu de ces derniers va dépendre de l'antispam que
vous utilisez. Ils serviront à apprendre à votre antispam si un message est
légitime ou non. Lisez la suite pour terminer.


====Scripts pour spamassassin====
Puisque nous utilisons spamassassin dans ce guide, voici comment remplir les
scripts d'apprentissages pour ce dernier.
Nous mettrons ces scripts dans le dossier ``/usr/local/lib/dovecot/sieve``.

Voici le contenu du fichier ``sa-learn-spam.sh``

```
#!/bin/sh
if [ $# -eq 1 ]; then
	exec /usr/local/bin/sa-learn -u ${1} --spam
fi
exit 0
```

Et ensuite le contenu du fichier ``sa-learn-ham.sh``

```
#!/bin/sh
if [ $# -eq 1 ]; then
	exec /usr/local/bin/sa-learn -u ${1} --ham
fi
exit 0
```

Afin que ces scripts puissent être lancés, rendez-les exécutables avec cette
commande : 

```
# chmod +x /usr/local/lib/dovecot/sieve/sa-learn-*.sh
```


%====Scripts pour rspamd====
%%%
sa-learn-spam.sh

exec /usr/bin/rspamc -h /run/rspamd/worker-controller.socket -P <secret> -u ${1} learn_spam

sa-learn-ham.sh

exec /usr/bin/rspamc -h /run/rspamd/worker-controller.socket -P <secret> -u ${1} learn_ham

%%%




Pour terminer, n'oubliez pas de recharger dovecot : 

```
# rcctl reload dovecot
```

Et voilà, désormais, les scripts d'apprentissages seront appelés lorsque les
utilisateurs déplaceront les mails dans leurs dossiers.
Si vous souhaitez en savoir plus, vous pouvez lire 
[cette page https://wiki2.dovecot.org/HowTo].

Héberger son serveur avec OpenBSD
L'auto-hébergement facile et sécurisé
Copyright © 2017 Xavier Cartron – %%mtime(%d/%m/%Y)
%! encoding: UTF-8
%! preproc: OBSDVERSION "6.1"
%! preproc: OBSDMINVERSION "61"
%! preproc: " OpenBSD " " [OpenBSD https://www.openbsd.org/] "
%! preproc: éditez "[éditez #editfile]"
%! preproc: éditer "[éditer #editfile]"
%! preproc: Éditez "[Éditez #editfile]"
%! preproc: " édite " " [édite #editfile] "
%! preproc: rediriger "[rediriger #router]"
%! preproc: redirigez "[redirigez #router]"
%! preproc: parefeu "[parefeu #pf]"
%! preproc: " logs" " [logs #logs]"
%! preproc: " firefox " " [firefox https://www.mozilla.org/fr/firefox/] "
%! preproc: "&NDD" "chezmoi.tld" 

%! preproc: "&IPV4" "192.0.2.2" 
%! preproc: "&LOCALIPV4" "192.168.1.2"
%! preproc: "&IPV6" "2001:db8:1:1::2" 
%! preproc: "&LOCALIPV6" "fd19:6f4d:bea8:f63a::2"
%! preproc: "&SSLKEY"    'key "/etc/ssl/acme/private/chezmoi.tld-privkey.pem"'
%! preproc: "&SSLCERT"   'certificate "/etc/ssl/acme/chezmoi.tld-fullchain.pem"'

%% HTML
%!preproc(html): "%HTMLINCLUDE" "%!include: htmlinclude.t2t"
%!preproc(html): "%HTMLISREAD" "%!include: htmlisread.t2t"
%!preproc(html): "%HTMLPAPIER" "%!include: htmlpapier.t2t"
%!preproc(html): "%HTMLCOVER" "[img/cover800600opti.png]"
%!postproc(html):  \^\^(.*?)\^\^  <sup>\1</sup>
%!postproc(html):  ,,(.*?),,      <sub>\1</sub>
%!postproc(html): "oe"	œ
%!postproc(html): "ublock"	"μblock"
%!postproc(html): ":\)"	"😁"
%!postproc(html): ";\)"	"😉"
%!postproc(html): "\^\^"	"😁"
% For lazy loading
%!postproc(html): "(<IMG.*)(SRC)(.*)>" '<img class="lazy-load" SRC="img/blank.gif" data-echo\3 /><noscript><img src\3 /></noscript>'
%!options(html): --css-sugar -n --toc --toc-level=4 --style css/style.css --style //cdn.jsdelivr.net/font-hack/2.020/css/hack.min.css

%% LATEX
%!options(tex): --toc --toc-level=4 -n
%!preproc(tex): "%LATEXINCLUDE" "%!include: latexinclude.t2t"
%!preproc(tex): "%LATEXURL" "%!include: latexurl.t2t"
%!preproc(tex): "%LATEXSMTPSCHEMA" "%!include: latexsmtp.t2t"
%!preproc(tex): "%LATEXEND" "%!include: latexend.t2t"
%!preproc(tex): "±"	"< + ou - >"
%!preproc(tex): ".png]"	"-gray.png]"
%!preproc(tex): ".jpg]"	"-gray.jpg]"
%!postproc(tex): "oeil"	\oe{il}
%!postproc(tex): "noeud"	n\oe{ud}
%!postproc(tex): "★"	"$\star$" 
%!postproc(tex): "ublock"	"$\\mu$block"
%!postproc(tex): "26\*26\*26\*26\*26\*26"	"$26 \\times 26 \\times 26 \\times 26 \\times 26 \\times 26$"
%!postproc(tex): "26\*26"	"$26 \\times 26$"
%!postproc(tex):  \\includegraphics{ \\vspace{2ex}\\noindent\\includegraphics[width=0.95\\textwidth]{
%!postproc(tex):  \\tableofcontents      \\newpage \\tableofcontents \\newpage 
%!postproc(tex):  \\documentclass{article} "\\documentclass[a5paper,twoside,10pt]{article}\n\\usepackage[francais]{babel} \n\\renewcommand{\\thesection}{\\arabic{section}}"
%!postproc(tex):  '(\\begin{verbatim})' '\\noindent\\begin{lstlisting}[breaklines]'
%!postproc(tex):  '(\\end{verbatim})'		'\\end{lstlisting}'
%!postproc(tex):  '(\\maketitle)'		'\\input{src/title.tex}'
%!postproc(tex):  '(quotation)'		'myquote'
%!postproc(tex):  '\\usepackage{geometry}'		'\\usepackage{geometry} \\geometry{top=2cm,bottom=2cm,outer=1.4cm,inner=1.7cm} \\geometry{bindingoffset=0.7cm} \\geometry{twoside} \\geometry{a5paper} \\geometry{marginratio={4:6,5:7}}'
%!postproc(tex):  '\\hypertarget{(.*?)}{}' '\\hypertarget{\1}{} \\label{\1}'
%!postproc(tex):  '\\htmladdnormallink{(.*?)}{\\#(.*?)}' '\\htmladdnormallink{\1}{\#\2}\\footnote{Voir page \\pageref{\2}.}'
%!postproc(tex):  '\\htmladdnormallink{(.*?)}{(http(s)?://(.*?))}' '\\htmladdnormallink{\1}{\2}\\footnote{\\htmladdnormallink{\2}{\2} }'

%require tikzsymbols
%!postproc(tex):  '\\section' '\\newpage\\section'
%!postproc(tex): ":\)"	"\\Smiley"
%!postproc(tex): ";\)"	"\\Winkey"
%!postproc(tex): "\^\^"	"\\Innocey"

%!style(tex): thumbpdf
%!style(tex): fancyhdr
%!style(tex): setspace
%!style(tex): listingsutf8
%!style(tex): geometry
%!style(tex): xcolor
%!style(tex): fancybox
%!style(tex): wrapfig
%!style(tex): tikzsymbols




Ce document est écrit pour vous aider à héberger chez vous certains services
malheureusement trop souvent confiés à des tiers. 

%LATEXINCLUDE

À la fin de la lecture, vous saurez :
- Héberger votre propre site web ;
- Héberger votre courrier électronique ;
- Mettre en place une sauvegarde régulière de vos données ;
- Profiter de vos propres applications web : cloud, forum, wiki...
- Héberger votre radio, un dispositif de surveillance vidéo, un VPN, un service caché TOR...
- Et bien d'autres choses comme l'indique le sommaire.


Notez que les procédures détaillées ici peuvent aussi être déployées sur des
serveurs dédiés. 

%HTMLCOVER

Vous pouvez consulter 
[les sources https://framagit.org/Thuban/auto-hebergement-openbsd] si vous
souhaitez encourager les contributeurs ou participer à ce document.
Pour contacter l'auteur, le soutenir, lui jeter des cailloux ou lui dire merci,
c'est [sur son site https://yeuxdelibad.net/Divers/Contact.html].

%HTMLPAPIER
%LATEXURL


Cet ouvrage est publié sous licence 
[CC-BY-SA http://creativecommons.org/licenses/by-sa/4.0/] à l'aide de 
[txt2tags http://txt2tags.org]. 





%HTMLISREAD
%%TOC


% Prerequis et installation
%!include: intro.t2t


%!include: prerequis.t2t


%!include: installation.t2t


% Administration
=Gérer son serveur=
%!include: admin.t2t


%!include: pf.t2t


%!include: ssh.t2t


%!include: sftp.t2t


%!include: maj.t2t


%!include: sauvegarde.t2t


% Services réseau
=Héberger un site web=
%!include: www.t2t


%!include: bdd.t2t


%!include: TP.t2t


=Héberger son courrier électronique=
%!include: mail.t2t



=Serveur de noms=[serveur-de-noms]
%!include: dns.t2t

% Autres services
=Services divers=
%!include: monitoring.t2t


%!include: syncthing.t2t


%!include: gopher.t2t


%!include: seedbox.t2t


%!include: cups.t2t


%!include: tor.t2t


%!include: nas.t2t


%!include: vpn.t2t


%!include: icecast.t2t


%!include: videosurveillance.t2t



=Aller plus loin=
%!include: plusloin.t2t


=Remarques complémentaires sur le système=

%!include: ssl.t2t

%!include: notes_diverses_obsd.t2t

=Annexes=

%!include: liens.t2t


%!include: faq.t2t


%!include: merci.t2t


%!include: fichiers.t2t


%LATEXEND

%HTMLINCLUDE

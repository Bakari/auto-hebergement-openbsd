Titre
Stéphane 22Decembre
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4

==Adresses réseau==[reseau]
===Rappel des concepts===

Pour construire un serveur, il vous faut impérativement configurer correctement sa configuration réseau. Vous avez en particulier besoin d'adresses (publiques et privées) stables, ainsi que de connaître l'adresse de votre passerelle ou routeur.

Cette documentation a pour but de vous aider à monter un serveur auto-hébergé, aussi on va se concentrer sur les solutions fournies par des fournisseurs d'accès Internet //grand public//.

====Adresse publique, adresse privée====

Tout d'abord sachez que votre fournisseur d'accès ne vous connecte sûrement pas //directement// sur le réseau internet, mais plutôt par ce qu'on appelle du //NAT//. Si vous utilisez une //box//, alors cet appareil (on va beaucoup parler de lui plus bas) va, lui, recevoir une adresse Internet //publique//.

Adresse publique, ça signifie que les gens qui naviguent sur le net peuvent contacter cette adresse. Elle est routable et unique. Tous les sites web notamment utilisent des adresses publiques.

Votre //box// va alors distribuer des adresses //privées// sur votre réseau local. (Ce qui signifie, si vous suivez bien, que vous avez, chez vous, plusieurs adresses privées - une pour chaque appareil connecté sur votre réseau : TV connectée, smartphone, imprimante… - qui se partagent **une seule adresse publique**.)

OK ? Bien.

Les adresses //privées// peuvent être dans trois //rangées//:

+ ``10.0.0.0/8`` : de 10.0.0.0 à 10.255.255.255.
+ ``172.16.0.0/12`` : de 172.16.0.0 à 172.31.255.255
+ ``192.168.0.0/16`` : de 192.168.0.0 à 192.168.255.255


Votre adresse //privée//, ou //locale//, c'est celle que votre ordinateur
utilise, celle qu'il connait. Par exemple sur un ordinateur utilisant Linux :

```
[toto@jabberwocky src]$ ip add show dev eno1
2: eno1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.0.153.194/16 brd 10.0.255.255 scope global dynamic eno1
       valid_lft 28073sec preferred_lft 28073sec
```


Ici ``10.0.153.194`` est bien une adresse (ou //IP// ou //adresse IP//) privée.

Votre adresse //publique//, c'est celle que vous utilisez pour la navigation
internet. C'est cette adresse que connaissent les différents sites web que vous
fréquentez. Et justement, si vous ne la connaissez pas, il vous suffit d'aller
voir par exemple [ifconfig.me http://ifconfig.me/].



====Le routeur====

Le routeur, c'est la //box// internet. On parle aussi de //passerelle// (//gateway// en anglais).

D'un côté du routeur, on a donc une adresse internet publique (que dans l'ensemble de cette documentation, on a supposé fixe, comme dit juste au dessus), de l'autre on a une adresse privée. Cette dernière adresse est très importante: c'est celle que votre serveur (ainsi que votre ordinateur personnel, votre smartphone, votre grille-pain connecté…) doit connaitre pour atteindre internet.

Dans les faits, quand quelqu'un dans votre réseau doit contacter un site internet, il //passera// par cette adresse, qui s'avère donc critique, et qui plus est un goulet d'étranglement.

C'est également le routeur qui fait les redirections de ports.

====Nom de machine====

Chaque élément sur le réseau a un nom (qu'on appelle //nom d'hôte//, ou //nom de machine//. Certaines documentation parlent de //nom de réseau de la machine//).

Sur les éléments les plus sophistiqués (les //vrais// ordinateurs), on peut changer ce nom. De nombreux appareils actuels sont toutefois vérouillés (essayez de changer le nom de machine d'un smartphone Android…). Ça n'a l'air de rien mais avoir un nom de machine permet de reconnaître les appareils //effectivement présents et fonctionnels// dans le réseau.

Le nom de machine d'un ordinateur tournant sous OpenBSD se définit dans ``/etc/myname``, où sera juste inscrit son nom complet. Par exemple:

```
toto@maitre:/home/toto cat /etc/myname
maitre.&NDD
```

====DHCP====

Pour configurer l'adresse //locale//, vous pouvez utiliser le client ``dhcp`` qui vient par défaut avec chaque installation OpenBSD.

Pour se faire, il suffit d'indiquer ``dhcp`` dans le fichier de configuration de l'interface réseau de votre serveur. Ce fichier est ``/etc/hostname.if`` avec **if** le nom de la dite interface. Si vous avez selectionné ``dhcp`` lors de l'installation d'OpenBSD, alors c'est bon.

```
toto@maitre:/home/toto cat /etc/hostname.re0                                                               
dhcp
```

Suivant la configuration du routeur que vous utilisez (configuration que vous ne pouvez pas forcement changer facilement), il peut être necessaire d'utiliser DHCP pour obtenir de la connectivité sur votre serveur (le routeur pourrait refuser de relayer les connections à une machine qui n'est pas dans sa table DHCP).

Si vous obtenez votre configuration réseau par DHCP, alors il n'est pas necessaire de connaître ou de renseigner l'adresse de votre routeur.

====Configuration statique====

La configuration //statique// d'une machine s'avère plus complèxe, mais on a davantage de maîtrise dessus (on peut par exemple assigner plusieurs adresses si on l'estime necessaire).

On configure d'abord l'interface voulue :

```
toto@maitre:/home/toto cat /etc/hostname.re0 
inet            &LOCALIPV4     255.255.255.0     192.168.0.255
inet   alias    192.168.0.9  255.255.255.0     192.168.0.255
et autant d'autres que vous le voulez…

up
```

Puis on indique l'adresse du routeur (son adresse privée, celle dont j'ai dit plus haut à quel point elle est importante). Cette adresse se configure dans le fichier ``/etc/mygate``:

```
toto@maitre:/home/toto cat /etc/mygate
192.168.0.1
```

Vous **devez** connaitre l'adresse de votre passerelle. Cette adresse n'est pas standard, en revanche les constructeurs ou les FAI ont souvent des réglages par défaut qui sont connus et qu'on peut retrouver sur le web. Il est également possible d'utiliser DHCP au début du processus de construction du serveur, puis mettre la configuration en statique quand on a pris la main et qu'on commence à vouloir faire des trucs sophistiqués.

Une fois ces deux fichiers configurés, l'interface sera configurée et mise en service automatiquement à chaque démarrage de la machine. Notez également que vous pouvez combiner des éléments de configuration dynamique (dhcp) avec de la configuration statique.

Vous pouvez tester votre bonne connexion au net grace à la (célèbre) commande ``ping``.

Tout d'abord pinger votre passerelle :

```
ping 192.168.0.1
```

Puis pinger les serveurs de Google :

```
ping 8.8.8.8
```

Si tout va bien, alors votre serveur est connecté au net.

===IPv6===

IPv6 c'est le //nouveau// protocole IP. Aujourd'hui, ça devient essentiel de le maîtriser un petit peu. Sachez que si vous n'avez pas encore d'ipv6 chez vous, il est probable que vous en ayez d'ici un ou deux ans. Mieux vaut être préparé pour en tirer le meilleur profit non ?

La configuration reprend les grands principes d'ipv4, mais avec des différences sur les méthodes et la syntaxe:

```
toto@maitre:/home/toto cat /etc/hostname.re0 
inet…

inet6           &IPV6       64 # adresse
inet6   autoconf    # utilisation des adresses SLAAC automatiques
rtsol               # écoute des annonces des routeurs

up
```

Les adresses SLAAC ont l'avantage d'être stables (si on utilise pas l'attribut //autoconfprivacy//), ce qui est exactement ce dont vous avez besoin sur un serveur. En revanche, elles sont encombrantes et peu mémorisables par un être humain.

Vous pouvez aussi utiliser les adresses ULA, qui sont des adresses locales pour votre réseau. Elles permettent de configurer un réseau de manière permanente sans avoir besoin de tout renuméroter en cas de changement de FAI. Elles sont également non-routables, ce qui veut dire que personne ne peut les //attaquer//. Enfin, elles constituent un excellent moyen de commencer à mettre en place votre réseau en attendant d'avoir vos adresses publiques définitives (si vous n'en avez pas encore). Vous pouvez lire davantage à propos de ces adresses [ici https://www.22decembre.eu/2016/12/02/ula-fr/].

Sachez qu'il existe un client dhcp pour ipv6, disponible dans les paquetages OpenBSD sous le nom //wide-dhcpv6//.

L'écoute des annonces des routeurs permet de connaître leur adresse, ce qui est, on vous le rapelle, une donnée essentielle. Vous pouvez aussi renseigner cette donnée dans le fichier ``/etc/mygate``.

```
toto@maitre:/home/toto cat /etc/mygate
192.168.0.1
2001:db8:1:1::
```

Ces divers paramètres se combinent de manière adéquate les uns avec les autres. Ainsi, vous pouvez tout à fait utiliser des adresses SLAAC //et// des adresses statiques.

En ipv6, il n'est plus question de redirection de ports: votre serveur est directement connecté à internet. Soyez donc prudent et vérifiez bien le pare-feu de votre serveur (PF).

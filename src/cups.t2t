Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8

==Serveur d'impression==
Nous allons voir dans ce chapitre comment relier une imprimante au serveur et la
rendre accessible afin de pouvoir imprimer de n'importe où depuis le 
[réseau local #reseau].
Ça peut être très pratique pour convertir une imprimante USB en imprimante
sans-fil. 

On commence par installer cups et des pilotes d'imprimante : 

```
# pkg_add cups cups-filters gutenprint foomatic-db
```

Pensez à ouvrir dans le parefeu le port 631 (ipp). Pour un réseau local, inutile
de le rediriger dans le routeur.


Maintenant, il faut autoriser l'accès à distance à l'interface d'administration
de l'imprimante (à moins que vous ne réalisiez toute la configuration
directement sur le serveur avec un navigateur en mode console comme w3m ou lynx). Pour cela,
modifiez de cette façon le fichier ``/etc/cups/cupsd.conf`` :

```
Listen 0.0.0.0:631
Listen /var/run/cups/cups.sock

# Restrict access to the server...
<Location />
Order deny,allow
Deny From All
Allow From 192.168.1.*
Allow From 127.0.0.1
Allow From @LOCAL
</Location>
```

Quelques explications :

- ``Deny From All`` : on interdit l'accès à tout le monde avant d'ajouter des
  exceptions.
- ``Allow From 192.168.1.*`` permet à tous les ordinateurs locaux dont
  l'adresse IP est de ce type de se connecter au serveur. 
  (* veut dire n'importe quel
  chiffre). Pour connaître cette IP, référez-vous 
  [au paragraphe qui explique ceci en détail #IP].
- ``Allow From 127.0.0.1`` permet l'accès en local. En fait, cette adresse est
  la même chose que “localhost”.


On active ensuite cups, puis on le démarre : 

```
# rcctl enable cupsd
# rcctl start cupsd
```

Maintenant, reliez votre imprimante sur le serveur. Ouvrez un
navigateur à l'adresse suivante : ``http://localhost:631``. Sur un serveur, cela
peut être avec w3m, un navigateur en console. Sinon utilisez votre ordinateur de
bureau, et indiquez l'adresse locale du serveur à la place de localhost. Cela
donnerait quelque chose comme ``http://&LOCALIPV4:631``.

Pour installer l'imprimante, allez dans “Administration, Ajouter une
imprimante” puis suivez les indications. Si le modèle de votre imprimante n'est
pas dans la liste, choisissez la version la plus proche.

**Important** : n'oubliez pas de cocher la case "Partager cette imprimante".

Finalement, vous pouvez ajouter cette imprimante sur les ordinateurs qui auront
besoin d'y accéder. Ouvrez un navigateur sur votre ordinateur puis allez à
l'adresse ``http://localhost:631/admin``. Suivez la procédure d'installation
d'imprimante, mais cette fois choisissez “Internet Printing Protocol (http)”.
Vous devez entrer l'adresse de l'imprimante.

Pour la trouver, affichez l'interface de CUPS du serveur, puis cliquez en haut à
droite sur “Imprimantes” et cliquez sur le nom de l'imprimante fraîchement
installée. Notez l'adresse qui est du type
``http://&LOCALIPV4:631/printers/HL2130``.

Vous devrez alors pour l'installer sur un ordinateur préciser une URL de ce type : 

```
ipp://&LOCALIPV4:631/printers/Brother_HL-2130_series
```


Cliquez sur “Continuer” et terminez l'installation. Et voilà, votre serveur
d'impression est prêt. 

===Note à propos des imprimantes USB===
La gestion des imprimantes USB sous OpenBSD est un peu particulière. Comme
décrit dans ``/usr/local/share/doc/pkg-readme/cups*``, il faut
autoriser l'accès au port USB pour l'utilisateur cups. Tapez alors la commande
``usbdevs -vd``. Vous obtenez une sortie de ce type : 

```
# usbdevs -vd
Controller /dev/usb0:
addr 1: high speed, self powered, config 1, EHCI root hub(0x0000), Intel(0x8086), rev 1.00
  uhub0
 port 1 addr 2: high speed, self powered, config 1, Rate Matching Hub(0x0024), Intel(0x8087), rev 0.00
   uhub2
  port 1 powered
  port 2 addr 3: low speed, power 100 mA, config 1, USB Mouse(0x1205), Genesys Logic(0x05e3), rev 1.00
    uhidev0
  port 3 powered
  port 4 powered
  port 5 powered
  port 6 powered
 port 2 powered
Controller /dev/usb1:
addr 1: high speed, self powered, config 1, EHCI root hub(0x0000), Intel(0x8086), rev 1.00
  uhub1
 port 1 addr 2: high speed, self powered, config 1, Rate Matching Hub(0x0024), Intel(0x8087), rev 0.00
   uhub3
  port 1 addr 3: high speed, self powered, config 1, HL-2130 series(0x003f), Brother(0x04f9), rev 1.00, iSerialNumber E2N507126
    ugen0
```


On remarque que l'imprimante (Brother) est identifiée par ``ugen0`` sur le
contrôleur ``/dev/usb1``.
Pour permettre à cups d'accéder à l'imprimante 
on lance la commande suivante :

```
# chown _cups /dev/ugen0.* /dev/usb1
```

Afin que ces modifications soient automatiques à chaque fois que l'imprimante
est reliée au serveur, installez le démon hotplugd puis activez-le : 

```
# pkg_add hotplug-diskmount
# rcctl enable hotplugd
# rcctl start hotplugd
```

Créez maintenant le script ``/etc/hotplug/attach`` pour y mettre : 

```
#!/bin/sh

DEVCLASS=$1
DEVNAME=$2

case $DEVCLASS in
0)
    if [ -n "$(echo $DEVNAME | grep -o "ugen[0-9]")" ]; then
        DEVDESCR=$(usbdevs -d | grep -B1 $DEVNAME | sed -Ee 's/ addr [0-9]+: (.+)$/\1/' -e 1q)
        if [ "${DEVDESCR}" == "HL-2130 series, Brother" ]; then
            chown _cups /dev/${DEVNAME}.* /dev/usb1
        fi
    fi
;;
2)
	# disk devices
	echo "Things for disks"
;;
esac

exit
```


Si votre imprimante n'était pas identifiée par quelque chose comme "ugen1" mais
"ulpt", alors il faut modifier la configuration du noyau pour désactiver ce
module. Cela se fait ainsi : 

```
# config -fe /bsd                                   
OpenBSD 6.0-stable (GENERIC.MP) #1: Fri Sep  2 10:41:52 CEST 2016
    root@&NDD: /usr/src/sys/arch/amd64/compile/GENERIC.MP
Enter 'help' for information
ukc> disable ulpt
291 ulpt* disabled
ukc> quit
Saving modified kernel.
```

Un redémarrage du serveur est nécessaire pour prendre les changements en compte.


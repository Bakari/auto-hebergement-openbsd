Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4

==Serveur de stockage==
Aussi appelé NAS, ces serveurs permettent de stocker et partager des fichiers
dans un réseau domestique. 

Pour mettre en place cette fonctionnalité, nous verrons deux solutions : l'une
basée sur SSH et l'autre sur NFS.

===Solution du fainéant : SSH===[nas]
Si vous avez déjà configuré [SSH #ssh], nous pouvons utiliser l'outil ``sshfs``
qui permet de monter dans un dossier les fichiers contenus sur le serveur, un
peu comme avec une clé USB. Cette solution fonctionnera quel que soit l'endroit
qui vous sert de point d'accès à Internet et restera sûr.

Admettons que vous souhaitez monter le dossier ``/mnt/partage`` du serveur dans
votre dossier ``/home/toto/serveur``. Vous utiliserez alors la commande suivante
: 


```
sshfs utilisateur_ssh@&NDD:/mnt/partage /home/toto/serveur
```


On demandera le mot de passe de l'utilisateur connecté via SSH, ensuite vous pourrez
voir les documents du serveur dans ``/home/toto/serveur`` comme s'ils étaient
sur votre ordinateur.

Notez que ce n'est pas toujours pratique d'avoir à entrer le mot de passe,
surtout lorsqu'on souhaite que le dossier du serveur soit monté automatiquement
à chaque démarrage. Vous pouvez dans ce cas consulter 
[l'authentification par clés #sshnopw].


===Solution un poil plus compliquée : NFS===

NFS veut dire "Network File System", autrement dit "Système de fichiers en
réseau". 

Cependant, nous n'allons pas ouvrir le partage NFS au monde entier comme on peut
le faire pour d'autres services, car nous ne pouvons pas restreindre l'accès à
certains utilisateurs. L'utilisation de NFS sera donc adaptée à
un partage local, c'est à dire pour toutes les machines utilisant votre routeur
pour se connecter.

%!include: nfs.t2t
